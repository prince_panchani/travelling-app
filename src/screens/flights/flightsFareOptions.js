import React from 'react';
import PropTypes from 'prop-types';
import {Text, TouchableOpacity, View} from 'react-native';
import {GlobalStyles} from '../../constants/style';
import Icons from '../../components/Icons';
import styles from './styles/flightsFareOptions';
import ReactNativeBiometrics from 'react-native-biometrics';

const FlightFareOptions = ({fareDetails, id, setFlightBookingDetails}) => {
  const {
    primaryText,
    colors: {black},
  } = GlobalStyles;

  const {price, fareOption, fareDetailsBySegment} = fareDetails ?? {};

  const isBiometricSupport = async fareId => {
    try {
      const rnBiometrics = new ReactNativeBiometrics();

      let response = await rnBiometrics.simplePrompt({
        promptMessage: 'Sign in with Touch ID',
        cancelButtonText: 'Close',
      });
      const {success} = response;
      setFlightBookingDetails({isLoggedIn: success, id: fareId});
    } catch (error) {
      console.log('|| Error:- ||', error);
    }
  };

  return (
    <>
      <View style={styles.container} key={id}>
        <View style={styles.fareOptionContainer}>
          <View style={styles.fareOptionHeader}>
            <View>
              <Text style={styles.fareOptionText}>{fareOption}</Text>
              <Text style={styles.generalText}>Fare offered by airline</Text>
            </View>
            <Text style={primaryText}>₹ {price?.total || 0}</Text>
          </View>
          {fareDetailsBySegment?.map(details => {
            const {amenities} = details ?? {};

            return (
              <View style={styles.fareOptionDetailsContainer}>
                {amenities?.map(amenity => (
                  <View style={styles.contentWrapper}>
                    <View style={styles.innerWrapper}>
                      <Icons name="shopping-bag" color={black} size={12} />
                      <Text style={styles.fareDetailsText}>
                        {amenity.description}
                      </Text>
                    </View>
                    <Text style={styles.statistics}>
                      {amenity.isChargeable ? 'Chargeable' : 'Not - Chargeable'}
                    </Text>
                  </View>
                ))}
              </View>
            );
          })}
          <TouchableOpacity
            style={styles.bookFlightContainer}
            onPress={() => isBiometricSupport(id)}>
            <View style={styles.innerFlightBookWrapper}>
              <Text style={styles.flightsBookText}>BOOK NOW</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

FlightFareOptions.propTypes = {
  fareDetails: PropTypes.shape({}),
  id: PropTypes.string,
  setFlightBookingDetails: PropTypes.func,
};

FlightFareOptions.defaultProps = {
  fareDetails: {},
  id: '',
  setFlightBookingDetails: () => {},
};

export default FlightFareOptions;
