import React, {useMemo, useState} from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import styles from './styles/searchedFlightList';
import {Flights} from '../../constants/constants';
import FlightFareOptions from './flightsFareOptions';
import BottomSheet from 'react-native-simple-bottom-sheet';

const SearchedFlightList = ({route}) => {
  // const {searchedFlights, carriers} = route?.params;
  const carriers = {
    WY: 'OMAN AIR',
    LX: 'ETIHAD AIRWAYS',
    AC: 'AIR CHINA',
  };
  const [activeFlightId, setActiveFlightId] = useState('');
  const [flightInfo, setFlightInfo] = useState(null);
  const [isLoggedInUser, setIsLoggedInUser] = useState(false);
  const [travelerId, setTravelerId] = useState('');

  const openFlightDetails = flightId => {
    setActiveFlightId(flightId);

    const selectedFlight = Flights.find(flight => flight.id === flightId);
    setFlightInfo(selectedFlight);
  };

  const setFlightBookingDetails = ({isLoggedIn, id}) => {
    setIsLoggedInUser(isLoggedIn);
    setTravelerId(id);
  };

  const chosenFlightClass = useMemo(() => {
    const selectedFareOption =
      flightInfo !== null &&
      travelerId &&
      flightInfo.travelerPricings.find(
        traveler => traveler.travelerId === travelerId,
      );

    return selectedFareOption;
  }, [travelerId, flightInfo]);

  return (
    <>
      <ScrollView
        style={styles.container}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}>
        {Flights.map((flightDetails, index) => {
          const {
            id,
            itineraries,
            price,
            validatingAirlineCodes,
            travelerPricings,
          } = flightDetails ?? {};
          const segment = itineraries[0].segments;
          const flightInfo = segment;
          const numberOfStops = flightInfo[0].numberOfStops;

          const airlineNames = validatingAirlineCodes.map(
            code => carriers[code],
          );

          return (
            <>
              <TouchableOpacity
                style={styles.flightInfoCard}
                key={index}
                onPress={() => openFlightDetails(id)}>
                <View style={styles.flightInfoCardHeader}>
                  <Image
                    source={require('../../assets/images/Swiss-International-Air-Lines-Emblem.png')}
                    style={styles.airlineImg}
                  />
                  <Text style={styles.airLineName}>{airlineNames}</Text>
                </View>
                <View style={styles.flightTimeInfoContainer}>
                  <View style={styles.departureTimeContainer}>
                    <Text style={styles.primaryText}>
                      {flightInfo[0].departure.at.slice(11, 16)}
                    </Text>
                    <Text style={styles.secondaryText}>
                      {flightInfo[0].departure.iataCode}
                    </Text>
                  </View>
                  <View style={styles.durationContainer}>
                    <Text style={styles.durationText}>2h 5m</Text>
                    <View style={styles.hLine} />
                    <Text style={styles.secondaryText}>
                      {numberOfStops === 0 ? 'Non stop' : numberOfStops}
                    </Text>
                  </View>
                  <View style={styles.dropTimeContainer}>
                    <Text style={styles.primaryText}>
                      {flightInfo[1].arrival.at.slice(11, 16)}
                    </Text>
                    <Text style={styles.secondaryText}>
                      {flightInfo[1].arrival.iataCode}
                    </Text>
                  </View>
                  <View style={styles.flightPriceContainer}>
                    <Text style={styles.primaryText}>
                      ₹ {price?.grandTotal}
                    </Text>
                    <Text style={styles.secondaryText}>per adult</Text>
                  </View>
                </View>
                <View style={styles.offerNotesContainer}>
                  <View style={styles.circleShape} />
                  <View>
                    <Text style={styles.offerText} numberOfLines={2}>
                      Get Rs 300 off using MMTBONUS & complimentary meal on this
                      flight + Additional upto 12% off on CITI cards using
                      MMTCITIFEST
                    </Text>
                  </View>
                </View>
                <View>
                  {travelerPricings.map(fareDetails => {
                    return (
                      <View>
                        {activeFlightId === id ? (
                          <FlightFareOptions
                            fareDetails={fareDetails}
                            id={id}
                            setFlightBookingDetails={setFlightBookingDetails}
                          />
                        ) : null}
                      </View>
                    );
                  })}
                </View>
              </TouchableOpacity>
            </>
          );
        })}
      </ScrollView>
      {flightInfo !== null && isLoggedInUser ? (
        <BottomSheet isOpen>
          {onScrollEndDrag => (
            <ScrollView onScrollEndDrag={onScrollEndDrag}>
              <View>
                <FlightFareOptions
                  fareDetails={chosenFlightClass}
                  id={chosenFlightClass.travelerId}
                  setFlightBookingDetails={setFlightBookingDetails}
                />
              </View>
            </ScrollView>
          )}
        </BottomSheet>
      ) : null}
    </>
  );
};

export default SearchedFlightList;
