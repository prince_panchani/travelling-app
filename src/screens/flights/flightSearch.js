import React, {useEffect, useState} from 'react';
import {
  Button,
  Image,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {GetFlights} from '../../services/services';
import styles from './styles/flightSearchStyle';
import {flightRequest} from '../../constants/constants';
import {GlobalStyles} from '../../constants/style';
import Icons from '../../components/Icons';
import {formatDate} from 'date-fns';
import {useRoute} from '@react-navigation/native';
import Loader from '../../components/Loader';

const FlightSearchScreen = ({navigation}) => {
  const {initialFlightParams} = flightRequest;
  const {
    colors: {gray200, black},
  } = GlobalStyles;

  const [searchRequest, setSearchRequest] = useState(initialFlightParams);
  const [isLoading, setIsLoading] = useState(false);

  const {
    originLocationCode,
    destinationLocationCode,
    departureDate,
    returnDate,
    adults,
  } = searchRequest ?? {};
  const route = useRoute();
  const originCode = route.params?.originLocationCode;
  const destinationCode = route.params?.destinationLocationCode;

  useEffect(() => {
    setSearchRequest(prev => ({
      ...prev,
      originLocationCode: originCode,
      destinationLocationCode: destinationCode,
    }));
  }, [destinationCode, originCode]);

  const {dateString: fromDate} = departureDate ?? {};
  const {dateString: toDate} = returnDate ?? {};

  const {
    name: originLocName,
    iataCode: originLocIataCode,
    detailedName: originLocDetailName,
  } = originLocationCode ?? {};
  const {
    name: destinationLocName,
    iataCode: destinationLocIataCode,
    detailedName: destinationLocDetailName,
  } = destinationLocationCode ?? {};

  const onChangeSearch = (type, value) => {
    setSearchRequest(prev => ({
      ...prev,
      [type]: value,
    }));
  };

  const searchFlight = async () => {
    try {
      setIsLoading(true);
      const flightSearchPayload = {
        ...searchRequest,
        originLocationCode: originLocationCode.iataCode,
        destinationLocationCode: destinationLocationCode.iataCode,
        departureDate: fromDate,
        returnDate: toDate,
      };

      const {
        data: {data, dictionaries},
      } = await GetFlights(flightSearchPayload);

      if (data && data.length) {
        navigation.navigate('SearchedFlightList', {
          searchedFlights: data,
          carriers: dictionaries.carriers,
        });
        setIsLoading(false);
      }
    } catch (error) {
      console.log('| Error:- Flight Search |', error.message);
    } finally {
      setIsLoading(false);
    }
  };

  const navigateToCalendarScreen = type => {
    navigation.navigate('CalendarScreen', {
      setSearchRequest,
      dateSelectionType: type,
    });
  };

  const navigateToFetchAirportScreen = () => {
    navigation.navigate('FetchAirport', {
      searchRequest,
      setSearchRequest,
    });
  };

  return (
    <ScrollView style={styles.container}>
      <View style={styles.offerContainer}>
        <View>
          <Image
            source={require('../../assets/images/percentage_icon.png')}
            style={styles.icon}
          />
        </View>
        <View>
          <Text style={styles.offerText}>
            Get FLAT 13% OFF on your first flight booking!
          </Text>
          <Text style={styles.offerText}>Use code: WELCOMEMMT</Text>
        </View>
      </View>

      <TouchableOpacity
        style={styles.inputBoxContainer}
        onPress={navigateToFetchAirportScreen}>
        <Image
          source={require('../../assets/images/takeoff.png')}
          style={styles.icon}
        />
        <View>
          <Text style={styles.inputHeaderText}>FROM</Text>
          <View style={styles.inputInnerWrapper}>
            <Text style={styles.generalText}>
              {originLocName || 'New Delhi'}
            </Text>
            <Text style={styles.secondaryText}>
              {originLocIataCode || 'DEL'}
            </Text>
          </View>
          <Text style={styles.secondaryText}>
            {originLocDetailName || 'Indira Gandhi International Airport'}
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.inputBoxContainer}
        onPress={navigateToFetchAirportScreen}>
        <Image
          source={require('../../assets/images/landon.png')}
          style={styles.icon}
        />
        <View>
          <Text style={styles.inputHeaderText}>To</Text>
          <View style={styles.inputInnerWrapper}>
            <Text style={styles.generalText}>
              {destinationLocName || 'Mumbai'}
            </Text>
            <Text style={styles.secondaryText}>
              {destinationLocIataCode || 'BOM'}
            </Text>
          </View>
          <Text style={styles.secondaryText}>
            {destinationLocDetailName ||
              'Chhatrapati Shivaji International Airport'}
          </Text>
        </View>
      </TouchableOpacity>

      <View style={styles.dateContainer}>
        <TouchableOpacity
          style={styles.dateInnerWrapper}
          onPress={() => navigateToCalendarScreen('departureDate')}>
          <Icons size={25} color={gray200} name="calendar" />
          <View>
            <Text style={styles.inputHeaderText}>DEPARTURE DATE</Text>
            <View style={styles.modifyDateContainer}>
              <Text style={styles.generalText}>
                {fromDate ? formatDate(fromDate, 'dd MMM') : ''}
              </Text>
              <Text style={styles.secondaryText}>
                {fromDate ? formatDate(fromDate, 'EEE, yyyy') : ''}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.dateInnerWrapper}
          onPress={() => navigateToCalendarScreen('returnDate')}>
          <Icons size={25} color={gray200} name="calendar" />
          <View>
            <Text style={styles.inputHeaderText}>Add RETURN DATE</Text>
            {!toDate ? (
              <Text style={styles.generalText}>Save more on round trips!</Text>
            ) : (
              <View style={styles.modifyDateContainer}>
                <Text style={styles.generalText}>
                  {fromDate ? formatDate(toDate, 'dd MMM') : ''}
                </Text>
                <Text style={styles.secondaryText}>
                  {fromDate ? formatDate(toDate, 'EEE, yyyy') : ''}
                </Text>
              </View>
            )}
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.inputBoxContainer}>
        <Icons size={25} color={gray200} name="child" />
        <View>
          <Text style={styles.inputHeaderText}>TRAVELERS</Text>
          <TextInput
            placeholder="Adults"
            keyboardType="numeric"
            onChangeText={adultsCount => onChangeSearch('adults', adultsCount)}
            style={{color: black}}
            placeholderTextColor={gray200}
            value={adults}
          />
        </View>
      </View>
      {isLoading ? (
        <Loader size="large" />
      ) : (
        <View style={styles.searchButtonWrapper}>
          <Button
            title="SEARCH FLIGHTS"
            onPress={searchFlight}
            accessibilityLabel="search flights"
          />
        </View>
      )}
    </ScrollView>
  );
};

export default FlightSearchScreen;
