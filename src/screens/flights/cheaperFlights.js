import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icons from '../../components/Icons';

const CheaperFlights = () => {
  return (
    <View style={styles.cheaperFlightsContainer}>
      <Text style={styles.cheaperCardHeader}>
        Looking For Cheaper Non-stop Flights?
      </Text>
      <View style={styles.cheaperFlightList}>
        <TouchableOpacity style={styles.cheaperFlightCard}>
          <Image
            source={require('../../assets/images/calendar.png')}
            style={styles.calendarImg}
          />
          <View>
            <Text style={styles.generalText}>
              Fly direct at cheaper rates on Fri, 16Feb
            </Text>
            <Text style={styles.secondaryText}>Flights start from $5, 376</Text>
          </View>
          <Icons name="chevron-right" color="#6D7BE6" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.cheaperFlightCard}>
          <Image
            source={require('../../assets/images/calendar.png')}
            style={styles.calendarImg}
          />
          <View>
            <Text style={styles.generalText}>
              Fly direct at cheaper rates on Fri, 16Feb
            </Text>
            <Text style={styles.secondaryText}>Flights start from $5, 376</Text>
          </View>
          <Icons name="chevron-right" color="#6D7BE6" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CheaperFlights;

const styles = StyleSheet.create({
  cheaperFlightsContainer: {
    backgroundColor: '#6D7BE6',
    margin: 8,
    padding: 8,
    borderRadius: 6,
  },
  cheaperCardHeader: {
    fontWeight: '900',
    color: '#fff',
    fontSize: 18,
  },
  cheaperFlightList: {
    flex: 1,
    gap: 10,
    padding: 8,
  },
  cheaperFlightCard: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 10,
    flexDirection: 'row',
    gap: 10,
    alignItems: 'center',
  },
  calendarImg: {
    resizeMode: 'cover',
    height: 25,
    width: 25,
  },
  generalText: {
    fontSize: 15,
    color: '#1EE696',
    fontWeight: '600',
  },
  secondaryText: {
    color: '#000',
  },
});
