import {StyleSheet} from 'react-native';
import {GlobalStyles} from '../../../constants/style';
import {Dimensions} from 'react-native';

const {
  colors: {primary300, black, white},
} = GlobalStyles;

const win = Dimensions.get('window');

const styles = StyleSheet.create({
  dateContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: 10,
    gap: 10,
    padding: 6,
    borderRadius: 10,
  },
  dateSelectContainer: {
    position: 'relative',
    flex: 1,
    borderWidth: 1,
    borderColor: primary300,
    borderRadius: 6,
    backgroundColor: '#E6E6E6',
    paddingLeft: 14,
    paddingVertical: 6,
    paddingRight: 6,
  },
  dateContainerText: {
    color: primary300,
    fontWeight: '600',
  },
  inputText: {
    color: black,
  },
  calendarContainer: {
    margin: 8,
    maxHeight: win.height - 220,
    borderWidth: 1,
    borderColor: '#65737e',
  },
  dateSelectBtn: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    marginTop: 10,
    backgroundColor: primary300,
  },
  btnText: {
    color: white,
  },
  monthContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    gap: 4,
  },
  monthName: {
    fontWeight: '800',
    color: black,
  },
  generalText: {
    fontSize: 18,
  },
  returnDateContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cancelIconContainer: {
    position: 'absolute',
    top: -8,
    right: -6,
  },
  emptyReturnDateContainer: {
    borderWidth: 1,
    borderColor: black,
    width: 8,
    marginVertical: 8,
  },
  modifyDateContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 4,
  },
  dateText: {
    color: '#000',
    fontWeight: '800',
  },
  dateSecondaryText: {
    color: '#65737e',
    fontWeight: '400',
  },
});

export default styles;
