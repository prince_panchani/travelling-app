import {StyleSheet} from 'react-native';
import {GlobalStyles} from '../../../constants/style';

const {
  colors: {black, white, primary800},
} = GlobalStyles;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  fareOptionContainer: {
    marginVertical: 4,
    backgroundColor: '#E6E6E6',
    padding: 8,
    borderRadius: 8,
  },
  fareOptionHeader: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  fareOptionText: {
    color: black,
    fontWeight: '600',
    fontSize: 16,
  },
  generalText: {
    fontSize: 12,
    color: black,
  },
  fareOptionDetailsContainer: {
    padding: 16,
  },
  contentWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 2,
    width: '100%',
  },
  innerWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 16,
    width: 120,
  },
  statistics: {
    marginLeft: 56,
    textAlign: 'left',
    color: black,
  },
  bookFlightContainer: {
    alignItems: 'flex-end',
  },
  innerFlightBookWrapper: {
    borderRadius: 16,
    backgroundColor: primary800,
    padding: 4,
  },
  flightsBookText: {
    color: white,
  },
  fareDetailsText: {
    color: black,
    fontSize: 12,
  },
});

export default styles;
