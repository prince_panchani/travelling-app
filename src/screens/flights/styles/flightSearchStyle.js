import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
  },
  offerContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    gap: 4,
    backgroundColor: '#4DEBA2',
    marginTop: 8,
    padding: 4,
    borderRadius: 8,
  },
  offerText: {
    fontWeight: '600',
    color: '#65737e',
  },
  icon: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  inputBoxContainer: {
    backgroundColor: '#E6E6E6',
    marginTop: 8,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: '#65737e',
    flexDirection: 'row',
    alignItems: 'center',
    gap: 10,
    padding: 6,
  },
  inputInnerWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 4,
  },
  inputHeaderText: {
    fontWeight: '700',
    color: '#65737e',
    fontSize: 14,
  },
  dateContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginTop: 8,
    gap: 6,
  },
  dateInnerWrapper: {
    backgroundColor: '#E6E6E6',
    flexDirection: 'row',
    alignItems: 'center',
    gap: 4,
    padding: 8,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: '#65737e',
    flex: 1,
  },
  generalText: {
    color: '#000',
    fontWeight: '800',
  },
  secondaryText: {
    color: '#65737e',
  },
  searchButtonWrapper: {
    marginTop: 8,
  },
  modifyDateContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 4,
  },
});

export default styles;
