import {StyleSheet} from 'react-native';
import {GlobalStyles} from '../../../constants/style';

const {
  colors: {black, white},
} = GlobalStyles;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 8,
  },
  flightInfoCard: {
    marginTop: 8,
    borderRadius: 10,
    padding: 8,
    backgroundColor: white,
  },
  flightInfoCardHeader: {
    flexDirection: 'row',
    gap: 4,
    alignItems: 'center',
  },
  airlineImg: {
    height: 30,
    width: 35,
  },
  airLineName: {
    fontWeight: '800',
    color: black,
  },
  offerNotesContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    gap: 4,
    backgroundColor: '#E0CEBA',
    marginTop: 8,
    padding: 4,
    borderRadius: 8,
  },
  circleShape: {
    height: 8,
    width: 8,
    backgroundColor: '#E17671',
    borderRadius: 25,
  },
  offerText: {
    fontWeight: '600',
    fontSize: 11,
  },
  flightTimeInfoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  primaryText: {
    fontWeight: '900',
    color: black,
    fontSize: 16,
  },
  secondaryText: {
    color: black,
    fontSize: 12,
  },
  durationContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  hLine: {
    borderWidth: 1,
    borderColor: '#4BDFE1',
    width: 60,
  },
  durationText: {
    fontSize: 12,
    color: '#65737e',
  },
});

export default styles;
