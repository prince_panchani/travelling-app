import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 8,
  },
  inputContainer: {
    marginTop: 8,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: '#BFBFBF',
    flexDirection: 'row',
    alignItems: 'center',
    gap: 6,
    paddingHorizontal: 6,
  },
  airportResultContainer: {
    marginTop: 8,
  },
  resultContainerHeading: {
    color: '#65737e',
    fontSize: 14,
    fontWeight: '800',
    marginLeft: 50,
  },
  airportInfoWrapper: {
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  innerWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 10,
  },
  image: {
    height: 35,
    width: 35,
  },
  airportCodeText: {
    fontWeight: '700',
    color: '#65737e',
    fontSize: 16,
  },
  loaderContainer: {
    flex: 1,
    marginTop: 20,
  },
  generalText: {
    color: '#000',
  },
  secondaryText: {
    color: '#65737e',
  },
  airportSearchText: {
    fontWeight: '800',
    fontSize: 18,
    paddingLeft: 2,
    width: 280,
  },
  landingPlane: {
    height: 25,
    width: 25,
  },
});

export default styles;
