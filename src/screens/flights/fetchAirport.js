import React, {useCallback, useEffect, useState} from 'react';
import {
  Image,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {GetAirportResult} from '../../services/services';
import styles from './styles/fetchAirport';
import {Debounce} from '../../helper';
import Loader from '../../components/Loader';
import {flightRequest} from '../../constants/constants';
import Icons from '../../components/Icons';
import {GlobalStyles} from '../../constants/style';

const FetchAirport = ({navigation, route}) => {
  const {searchRequest} = route.params;
  const {
    colors: {black, gray200},
  } = GlobalStyles;

  const {originLocationCode, destinationLocationCode} = searchRequest ?? {};
  const {name: originLocName, iataCode: originLocIataCode} =
    originLocationCode ?? {};
  const {name: destinationLocName, iataCode: destinationLocIataCode} =
    originLocationCode ?? {};

  const {
    journeyType: {origin, destination},
  } = flightRequest;
  const [searchAirportCode, setSearchAirportCode] = useState({
    originAirportCode: '',
    destinationAirportCode: '',
  });

  const [airportResult, setAirportResult] = useState({
    originAirports: [],
    destinationAirports: [],
  });
  const [isLoading, setIsLoading] = useState(false);
  const [activeJourneyType, setActiveJourneyType] = useState(origin);

  const {originAirportCode, destinationAirportCode} = searchAirportCode ?? {};
  const {originAirports, destinationAirports} = airportResult ?? [];

  const fetchSearchResult = async (type, searchQuery) => {
    try {
      setIsLoading(true);
      if (!searchQuery) {
        return;
      }

      const {
        data: {data},
      } = await GetAirportResult(searchQuery);
      if (data.length) {
        setAirportResult(prev => ({
          ...prev,
          [type === 'originAirportCode'
            ? 'originAirports'
            : 'destinationAirports']: data,
        }));
      }
    } catch (error) {
      console.log('error', error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (originLocationCode) {
      setSearchAirportCode(prev => ({
        ...prev,
        originAirportCode: originLocName,
      }));
    } else if (destinationLocationCode) {
      setSearchAirportCode(prev => ({
        ...prev,
        destinationAirportCode: destinationLocName,
      }));
    } else {
      setSearchAirportCode({
        originAirportCode: '',
        destinationAirportCode: '',
      });
    }
  }, [
    destinationLocName,
    destinationLocationCode,
    originLocName,
    originLocationCode,
  ]);

  useEffect(() => {
    if (originLocationCode && activeJourneyType === origin) {
      fetchSearchResult('originAirportCode', originLocIataCode);
    } else if (destinationLocationCode) {
      fetchSearchResult('destinationAirportCode', destinationLocIataCode);
    } else {
      fetchSearchResult('originAirportCode', originAirportCode);
    }
  }, [
    activeJourneyType,
    destinationLocIataCode,
    destinationLocationCode,
    origin,
    originAirportCode,
    originLocIataCode,
    originLocationCode,
  ]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debounceSearch = useCallback(
    Debounce((type, searchValue) => {
      fetchSearchResult(type, searchValue);
    }, 500),
    [],
  );

  const handleFlightSearch = (type, value) => {
    setSearchAirportCode(prev => ({
      ...prev,
      [type]: value.toUpperCase(),
    }));
    debounceSearch(type, value.toUpperCase());
  };

  const setJourney = type => {
    setActiveJourneyType(type);
  };

  const navigateToFlightSearchScreen = airPortId => {
    if (activeJourneyType === origin) {
      const airportInfo =
        originAirports && originAirports.find(({id}) => id === airPortId);
      navigation.navigate('FlightSearch', {
        originLocationCode: airportInfo,
      });
    } else {
      const airportInfo =
        destinationAirports &&
        destinationAirports.find(({id}) => id === airPortId);
      navigation.navigate('FlightSearch', {
        destinationLocationCode: airportInfo,
      });
    }
  };

  const navigateToBackScreen = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.inputContainer}
        onPress={() => {
          setJourney(origin);
          setSearchAirportCode(prev => ({
            ...prev,
            originAirportCode: '',
          }));
        }}>
        <Icons
          name="arrow-left"
          color={black}
          size={25}
          onPress={navigateToBackScreen}
        />
        <View>
          <Text style={[styles.generalText, styles.airportSearchText]}>
            From
          </Text>
          <TextInput
            placeholder="Enter any Airport Name"
            onChangeText={originCode =>
              handleFlightSearch('originAirportCode', originCode)
            }
            value={originAirportCode}
            placeholderTextColor={gray200}
            style={[styles.generalText, styles.airportSearchText]}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.inputContainer}
        onPress={() => setJourney(destination)}>
        <Image
          source={require('../../assets/images/landon.png')}
          style={styles.landingPlane}
        />
        <View>
          <Text style={[styles.generalText, styles.airportSearchText]}>To</Text>
          <TextInput
            placeholder="Enter any Airport Name"
            onChangeText={destinationCode =>
              handleFlightSearch('destinationAirportCode', destinationCode)
            }
            value={destinationAirportCode}
            placeholderTextColor={gray200}
            style={[styles.generalText, styles.airportSearchText]}
          />
        </View>
      </TouchableOpacity>

      <ScrollView style={styles.airportResultContainer}>
        <Text style={styles.resultContainerHeading}>SUGGESTIONS</Text>
        {isLoading ? (
          <View style={styles.loaderContainer}>
            <Loader size="large" />
          </View>
        ) : activeJourneyType === origin && originAirports ? (
          originAirports.map(airport => {
            const {name, detailedName, iataCode, id} = airport ?? {};

            return (
              <TouchableOpacity
                style={styles.airportInfoWrapper}
                key={id}
                onPress={() => navigateToFlightSearchScreen(id)}>
                <View style={styles.innerWrapper}>
                  <Image
                    source={require('../../assets/images/takeoff.png')}
                    style={styles.image}
                  />
                  <View>
                    <Text style={styles.generalText}>{name}</Text>
                    <Text style={styles.secondaryText}>{detailedName}</Text>
                  </View>
                </View>
                <Text style={styles.airportCodeText}>{iataCode}</Text>
              </TouchableOpacity>
            );
          })
        ) : (
          activeJourneyType === destination &&
          destinationAirports &&
          destinationAirports.map(airport => {
            const {name, detailedName, iataCode, id} = airport ?? {};

            return (
              <TouchableOpacity
                style={styles.airportInfoWrapper}
                key={id}
                onPress={() => navigateToFlightSearchScreen(id)}>
                <View style={styles.innerWrapper}>
                  <Image
                    source={require('../../assets/images/landon.png')}
                    style={styles.image}
                  />
                  <View>
                    <Text style={styles.generalText}>{name}</Text>
                    <Text style={styles.secondaryText}>{detailedName}</Text>
                  </View>
                </View>
                <Text style={styles.airportCodeText}>{iataCode}</Text>
              </TouchableOpacity>
            );
          })
        )}
      </ScrollView>
    </View>
  );
};

export default FetchAirport;
