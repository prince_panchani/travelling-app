import React, {useCallback, useEffect, useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {CalendarList} from 'react-native-calendars';
import {calendarTheme} from '../../constants/constants';
import styles from './styles/calendarScreen';
import Icons from '../../components/Icons';
import {formatDate} from '../../helper';

const CalendarScreen = ({navigation, route}) => {
  const {setSearchRequest, dateSelectionType} = route.params;
  const currentDate = new Date().toISOString().split('T')[0];
  const [dateType, setDateType] = useState(dateSelectionType);

  const [selectedDate, setSelectedDate] = useState({
    departureDate: {dateString: currentDate},
    returnDate: '',
  });

  const {departureDate, returnDate} = selectedDate ?? {};
  const {dateString: fromDate} = departureDate ?? {};
  const {dateString: toDate} = returnDate ?? {};

  const renderHeader = useCallback(item => {
    const date = new Date(item);
    const month = date.toLocaleString('default', {month: 'long'});
    const year = date.getFullYear();

    return (
      <TouchableOpacity style={styles.monthContainer}>
        <Text style={[styles.monthName, styles.generalText]}>{month}</Text>
        <Text style={styles.generalText}>{year}</Text>
      </TouchableOpacity>
    );
  }, []);

  const handleDateSelection = type => {
    setDateType(type);
  };

  const handleDate = () => {
    setSearchRequest(prev => ({
      ...prev,
      departureDate: departureDate,
      returnDate: returnDate,
    }));
    navigation.goBack();
  };

  const handleDayPress = selectedDay => {
    setSelectedDate(prevState => ({...prevState, [dateType]: selectedDay}));
  };

  const dateToStr = date => {
    let day = date.getDate().toString().padStart(2, '0');
    let month = (date.getMonth() + 1).toString().padStart(2, '0');
    let year = date.getFullYear();
    return year + '-' + month + '-' + day;
  };

  const getMarked = () => {
    let marked = {};
    if (fromDate && toDate) {
      let from = new Date(fromDate);
      let to = new Date(toDate);
      for (let d = new Date(from); d <= to; d.setDate(d.getDate() + 1)) {
        let currentDateStr = dateToStr(d);
        marked[currentDateStr] = {
          startingDay: currentDateStr === fromDate,
          endingDay: currentDateStr === toDate,
          color: '#00aeff',
          textColor: '#fff',
        };
      }
    } else {
      marked[fromDate] = {
        color: '#00aeff',
        textColor: '#fff',
      };
    }

    return marked;
  };

  useEffect(() => {
    navigation.setOptions({
      title:
        dateType === 'departureDate'
          ? 'Select Departure Date'
          : 'Select Return Date',
    });
  }, [dateType, navigation]);

  return (
    <View>
      <View style={styles.dateContainer}>
        <TouchableOpacity
          style={styles.dateSelectContainer}
          onPress={() => handleDateSelection('departureDate')}>
          <Text style={styles.dateContainerText}>DEPARTURE DATE</Text>
          <View style={styles.modifyDateContainer}>
            <Text style={styles.dateText}>
              {fromDate ? formatDate(fromDate, 'dd MMM') : ''}
            </Text>
            <Text style={styles.dateSecondaryText}>
              {fromDate ? formatDate(fromDate, 'EEE, yyyy') : ''}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.dateSelectContainer}
          onPress={() => handleDateSelection('returnDate')}>
          <View style={styles.returnDateContainer}>
            <Icons name="plus" color="#00aeff" size={10} />
            <Text style={styles.dateContainerText}>ADD RETURN DATE</Text>
          </View>
          {dateType !== 'returnDate' ? (
            <Text style={styles.inputText}>Save more on round trips!</Text>
          ) : toDate ? (
            <View style={styles.modifyDateContainer}>
              <Text style={styles.dateText}>
                {fromDate ? formatDate(toDate, 'dd MMM') : ''}
              </Text>
              <Text style={styles.dateSecondaryText}>
                {fromDate ? formatDate(toDate, 'EEE, yyyy') : ''}
              </Text>
            </View>
          ) : (
            <View style={styles.emptyReturnDateContainer} />
          )}
          {dateType === 'returnDate' ? (
            <View style={styles.cancelIconContainer}>
              <Icons name="times-circle" color="#65737e" size={18} />
            </View>
          ) : null}
        </TouchableOpacity>
      </View>
      <View style={styles.calendarContainer}>
        <CalendarList
          onDayPress={day => handleDayPress(day)}
          // Max amount of months allowed to scroll to the past. Default = 50
          pastScrollRange={12}
          // Max amount of months allowed to scroll to the future. Default = 50
          futureScrollRange={12}
          // Enable or disable scrolling of the calendar list
          scrollEnabled={true}
          accessibilityRole={'adjustable'}
          accessible
          initialDate={currentDate}
          markingType="period"
          markedDates={getMarked()}
          renderHeader={renderHeader}
          theme={calendarTheme}
        />
        <TouchableOpacity onPress={handleDate} style={styles.dateSelectBtn}>
          <Text style={styles.btnText}>Done</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CalendarScreen;
