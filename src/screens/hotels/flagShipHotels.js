import React from 'react';
import {Image, Text, View} from 'react-native';
import styles from './styles/flagShipHotels';

const FlagShipHotels = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>Flagship Hotel Stores</Text>
      <View>
        <View style={styles.hotelListContainer}>
          <View>
            <View style={styles.imageWrapper}>
              <Image
                source={require('../../assets/images/hotel5.jpg')}
                style={styles.hotelImage}
              />
            </View>
            <View style={styles.hotelNameWrapper}>
              <Text style={styles.hotelName}>The Clark Hotels</Text>
              <Text style={styles.hotelName}>& Resorts</Text>
            </View>
            <View style={styles.hotelCompanyLogo}>
              <Image
                source={require('../../assets/images/luxury-hotel-logo.jpg')}
                style={styles.hotelCompanyLogoImage}
              />
            </View>
          </View>
          <View>
            <View style={styles.imageWrapper}>
              <Image
                source={require('../../assets/images/hotel6.jpg')}
                style={styles.hotelImage}
              />
            </View>
            <View style={styles.hotelNameWrapper}>
              <Text style={styles.hotelName}>The Fern Hotels &</Text>
              <Text style={styles.hotelName}>Resorts</Text>
            </View>
            <View style={styles.hotelCompanyLogo}>
              <Image
                source={require('../../assets/images/hotel_company_logo2.jpg')}
                style={styles.hotelCompanyLogoImage}
              />
            </View>
          </View>
        </View>
        <View style={styles.hotelListContainer}>
          <View>
            <View style={styles.imageWrapper}>
              <Image
                source={require('../../assets/images/hotel7.jpg')}
                style={styles.hotelImage}
              />
            </View>
            <View style={styles.hotelNameWrapper}>
              <Text style={styles.hotelName}>Inde Hotels &</Text>
              <Text style={styles.hotelName}>Resorts</Text>
            </View>
            <View style={styles.hotelCompanyLogo}>
              <Image
                source={require('../../assets/images/hotel_company_logo3.jpg')}
                style={styles.hotelCompanyLogoImage}
              />
            </View>
          </View>
          <View>
            <View style={styles.imageWrapper}>
              <Image
                source={require('../../assets/images/hotel4.jpg')}
                style={styles.hotelImage}
              />
            </View>
            <View style={styles.hotelNameWrapper}>
              <Text style={styles.hotelName}>Wyndham Hotels</Text>
              <Text style={styles.hotelName}>& Resorts</Text>
            </View>
            <View style={styles.hotelCompanyLogo}>
              <Image
                source={require('../../assets/images/hotel_company_logo.jpg')}
                style={styles.hotelCompanyLogoImage}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default FlagShipHotels;
