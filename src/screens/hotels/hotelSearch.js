import React, {useState} from 'react';
import {Button, TextInput, View} from 'react-native';
import {GetHotelByCityCode} from '../../services/services';
import styles from './styles/hotelSearch';

const HotelSearchScreen = ({navigation}) => {
  const [hotelSearchReQuest, setHotelSearchRequest] = useState({
    cityCode: '',
    checkInDate: '',
    checkOutDate: '',
    adults: 0,
  });

  const {cityCode, adults} = hotelSearchReQuest;

  const onChangeSearch = (type, value) => {
    setHotelSearchRequest(prev => ({
      ...prev,
      [type]: value,
    }));
  };

  const searchHotels = async () => {
    try {
      const response = await GetHotelByCityCode(hotelSearchReQuest);
      console.log('response', response);

      navigation.navigate('SearchedHotelList', {searchedHotels: response});
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Enter city code"
        style={styles.inputBox}
        onChangeText={code => onChangeSearch('cityCode', code)}
      />
      <TextInput
        placeholder="Adults"
        keyboardType="numeric"
        onChangeText={adultsCount => onChangeSearch('adults', adultsCount)}
        style={styles.inputBox}
      />
      <View style={styles.searchButtonWrapper}>
        <Button
          title="SEARCH"
          onPress={searchHotels}
          accessibilityLabel="search flights"
        />
      </View>
    </View>
  );
};

export default HotelSearchScreen;
