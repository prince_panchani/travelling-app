import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 8,
  },
  inputBox: {
    padding: 10,
    marginTop: 8,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: '#65737e',
  },
  searchButtonWrapper: {
    marginTop: 8,
  },
});

export default styles;
