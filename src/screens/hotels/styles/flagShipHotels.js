import {StyleSheet} from 'react-native';
import {GlobalStyles} from '../../../constants/style';
import {Dimensions} from 'react-native';

const {
  colors: {black, white},
} = GlobalStyles;
const win = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 8,
  },
  headerText: {
    fontWeight: '900',
    fontSize: 18,
    color: black,
  },
  hotelListContainer: {
    flexDirection: 'row',
    marginTop: 4,
    justifyContent: 'space-between',
  },
  hotelImage: {
    resizeMode: 'cover',
    height: 120,
    width: win.width / 2,
    borderRadius: 8,
    flex: 1,
  },
  imageWrapper: {
    position: 'relative',
  },
  hotelNameWrapper: {
    position: 'absolute',
    bottom: 8,
    left: 8,
  },
  hotelCompanyLogo: {
    position: 'absolute',
    top: 8,
    left: 8,
  },
  hotelCompanyLogoImage: {
    borderRadius: 50,
    height: 40,
    width: 40,
  },
  hotelName: {
    color: white,
    fontWeight: '800',
    fontSize: 18,
  },
});

export default styles;
