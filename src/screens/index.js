import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Image, StyleSheet} from 'react-native';
import TripHome from './bottomTabScreens/tripHome';
import MyTripScreen from './bottomTabScreens/myTrip';

const Tab = createBottomTabNavigator();

const HomeScreen = () => {
  const renderMyTripLogo = () => (
    <Image
      source={require('../assets/images/mobile_logo.png')}
      style={styles.image}
    />
  );

  const renderMyTripsTabIcon = () => (
    <Image source={require('../assets/images/bag.png')} style={styles.image} />
  );

  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Home"
        component={TripHome}
        options={{
          tabBarIcon: renderMyTripLogo,
        }}
      />
      <Tab.Screen
        name="My Trips"
        component={MyTripScreen}
        options={{
          tabBarIcon: renderMyTripsTabIcon,
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  image: {
    height: 34,
    width: 34,
    resizeMode: 'contain',
  },
});

export default HomeScreen;
