import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
// import FlagShipHotels from '../hotels/flagShipHotels';
import {GlobalStyles} from '../../constants/style';

const TripHome = ({navigation}) => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <TouchableOpacity onPress={() => navigation.navigate('FlightSearch')}>
          <View style={styles.card}>
            <Image
              source={require('../../assets/images/plane.png')}
              style={styles.image}
            />
            <Text style={styles.cardText}>Flights</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('HotelSearch')}>
          <View style={styles.card}>
            <Image
              source={require('../../assets/images/hotel.png')}
              style={styles.image}
            />
            <Text style={styles.cardText}>Hotels</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('HotelSearch')}>
          <View style={styles.card}>
            <Image
              source={require('../../assets/images/holidays.png')}
              style={styles.image}
            />
            <Text style={styles.cardText}>Holiday Packages</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('HotelSearch')}>
          <View style={styles.card}>
            <Image
              source={require('../../assets/images/bus-train.png')}
              style={styles.image}
            />
            <Text style={styles.cardText}>Trains/Bus</Text>
          </View>
        </TouchableOpacity>
      </View>
      {/* <FlagShipHotels /> */}
    </ScrollView>
  );
};

const {
  colors: {black},
} = GlobalStyles;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginTop: 8,
    paddingHorizontal: 8,
  },
  card: {
    height: 86,
    width: 80,
    backgroundColor: '#E1F2FD',
    borderRadius: 10,
    alignItems: 'center',
  },
  image: {
    height: 26,
    width: 30,
    marginTop: 16,
    resizeMode: 'contain',
  },
  cardText: {
    textAlign: 'center',
    fontWeight: '800',
    color: black,
  },
});

export default TripHome;
