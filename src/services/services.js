import {BaseURL} from '../config';
import api from '../interceptor';

export const GetHotelByCityCode = cityCode => {
  const url = `${BaseURL}reference-data/locations/hotels/by-city?cityCode=${cityCode}&radius=5&radiusUnit=KM&hotelSource=ALL`;

  return api.get(url);
};

export const GetHotelDetails = hotelIds => {
  const url = `https://test.api.amadeus.com/v3/shopping/hotel-offers?hotelIds=${hotelIds}&adults=1`;

  return api.get(url);
};

/* Flight search */

export const GetFlights = ({
  originLocationCode,
  destinationLocationCode,
  departureDate,
  returnDate,
  adults,
}) => {
  const url = `https://test.api.amadeus.com/v2/shopping/flight-offers?originLocationCode=${originLocationCode}&destinationLocationCode=${destinationLocationCode}&departureDate=${departureDate}&returnDate=${returnDate}&adults=${adults}&max=5`;

  return api.get(url);
};

export const GetCheaperFlights = ({
  originLocationCode,
  destinationLocationCode,
}) => {
  const url = `${BaseURL}shopping/flight-dates?origin=${originLocationCode}&destination=${destinationLocationCode}&duration=1`;

  return api.get(url);
};

export const GetFlightOfferPricing = () => {
  const url = `${BaseURL}shopping/flight-offers/pricing`;

  return api.get(url);
};

export const GetAirportResult = airportCode => {
  const url = `${BaseURL}reference-data/locations?subType=CITY,AIRPORT&keyword=${airportCode}`;

  return api.get(url);
};

export const GetNearestAirport = (latitude, longitude) => {
  const url = `${BaseURL}reference-data/locations/airports?latitude=${latitude}&longitude=${longitude}`;

  return api.get(url);
};
