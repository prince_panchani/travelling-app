import {API_URL, CLIENT_ID, CLIENT_SECRET, GRANT_TYPE} from '@env';

export const BaseURL = `${API_URL}`;

export const requestTokenURL = `${BaseURL}/security/oauth2/token`;

export const credentialsConfig = {
  client_id: CLIENT_ID,
  client_secret: CLIENT_SECRET,
  grant_type: GRANT_TYPE,
};
