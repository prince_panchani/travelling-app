import axios from 'axios';
import {BaseURL, requestTokenURL, credentialsConfig} from './config';
import AsyncStorage from '@react-native-async-storage/async-storage';

// Flag to prevent multiple token refresh requests
let isRefreshing = false;

// list to hold the request queue
let failedQueue = [];

const api = axios.create({
  baseURL: BaseURL,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
  withCredentials: true,
  responseType: 'json',
});

const refreshAccessToken = async () => {
  const response = await axios.post(requestTokenURL, credentialsConfig, {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  });
  const access_token = response.data.access_token;
  await AsyncStorage.setItem('access_token', access_token);

  return access_token;
};

// Request interceptor
api.interceptors.request.use(
  async config => {
    let token = await AsyncStorage.getItem('access_token');
    let accessToken = token ? `Bearer ${token}` : null;

    if (accessToken) {
      if (config.headers) {
        config.headers.Authorization = accessToken;
      }
    }

    return config;
  },
  error => {
    return Promise.reject(error);
  },
);

// Response interceptor
api.interceptors.response.use(
  response => response,
  async error => {
    const originalRequest = error.config;

    if (
      error.response &&
      (error.response.status === 401 || error.response.status === 400)
    ) {
      if (!isRefreshing) {
        isRefreshing = true;
        try {
          const newAccessToken = refreshAccessToken();

          error.config.headers.Authorization = `Bearer ${newAccessToken}`;

          // Retry all requests in the queue with the new token
          failedQueue.forEach(({config, resolve, reject}) => {
            api
              .request(config)
              .then(response => resolve(response.data))
              .catch(err => reject(err));
          });

          // Clear the queue
          failedQueue.length = 0;

          // Retry the original request
          return api(originalRequest);
        } catch (refreshError) {
          // You can clear all storage and redirect the user to the login page
          throw refreshError;
        } finally {
          isRefreshing = false;
        }
      }

      // Add the original request to the queue
      return new Promise((resolve, reject) => {
        failedQueue.push({config: originalRequest, resolve, reject});
      });
    }

    // Return a Promise rejection if the status code is not 401
    return Promise.reject(error);
  },
);

export default api;
