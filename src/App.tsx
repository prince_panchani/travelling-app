import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import FlightSearchScreen from './screens/flights/flightSearch';
import HotelSearchScreen from './screens/hotels/hotelSearch';
import HomeScreen from './screens';

/* provides native-driven gesture management APIs for building best possible touch-based experiences in React Native.
With this library gestures are no longer controlled by the JS responder system,
but instead are recognized and tracked in the UI thread.
It makes touch interactions and gesture tracking not only smooth, but also dependable and deterministic */
import 'react-native-gesture-handler';
import {StatusBar, StyleSheet, useColorScheme} from 'react-native';
import SearchedFlightList from './screens/flights/searchedFlightList';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import SearchedHotelList from './screens/hotels/searchedHotels';
import CalendarScreen from './screens/flights/calendarScreen';
import FetchAirport from './screens/flights/fetchAirport';
import Splash from './screens/splash/splash';

const Stack = createNativeStackNavigator();

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  return (
    <GestureHandlerRootView style={styles.container}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="HomeScreen"
            component={HomeScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="FlightSearch"
            component={FlightSearchScreen}
            options={{title: 'Flight Search'}}
          />
          <Stack.Screen
            name="FetchAirport"
            component={FetchAirport}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="SearchedFlightList"
            component={SearchedFlightList}
          />
          <Stack.Screen
            name="CalendarScreen"
            component={CalendarScreen}
            options={{title: 'Select Departure Date'}}
          />
          <Stack.Screen
            name="SearchedHotelList"
            component={SearchedHotelList}
          />
          <Stack.Screen
            name="HotelSearch"
            component={HotelSearchScreen}
            options={{title: 'Hotel & Homestays'}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </GestureHandlerRootView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
