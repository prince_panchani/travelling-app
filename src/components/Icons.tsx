import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

type IconsProps = React.PropsWithChildren<{
  name: string;
  color: string;
  size: number;
}>;

const Icons = ({name, color, size, ...rest}: IconsProps) => {
  return <Icon name={name} size={size} color={color} {...rest} />;
};

export default Icons;
