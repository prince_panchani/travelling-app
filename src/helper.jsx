import {format} from 'date-fns';

export const Debounce = function (func, wait, immediate) {
  let timeout;
  return function () {
    const context = this;
    const args = arguments;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

export const formatDate = (date, pattern) => {
  let modifiedDate = null;
  const timestamp = Date.parse(date);

  if (!Number.isNaN(timestamp)) {
    const newDate = new Date(timestamp);
    modifiedDate = format(newDate, pattern);
  }

  return modifiedDate;
};
