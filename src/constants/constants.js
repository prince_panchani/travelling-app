import {GlobalStyles} from './style';

const {
  colors: {gray100},
} = GlobalStyles;

const currentDate = new Date().toISOString().split('T')[0];

export const calendarTheme = {
  'stylesheet.calendar.main': {
    dayContainer: {
      borderColor: gray100,
      borderWidth: 1,
      flex: 1,
      padding: 10,
    },
    emptyDayContainer: {
      borderColor: gray100,
      backgroundColor: gray100,
      borderWidth: 1,
      flex: 1,
      padding: 10,
    },
    week: {
      marginTop: 0,
      marginBottom: 0,
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
  },
};

export const flightRequest = {
  initialFlightParams: {
    originLocationCode: null,
    destinationLocationCode: null,
    departureDate: {dateString: currentDate},
    returnDate: null,
    adults: '1',
  },
  journeyType: {
    origin: 'ORIGIN',
    destination: 'DESTINATION',
  },
};

export const Flights = [
  {
    type: 'flight-offer',
    id: '1',
    source: 'GDS',
    instantTicketingRequired: false,
    nonHomogeneous: false,
    oneWay: false,
    lastTicketingDate: '2024-02-20',
    lastTicketingDateTime: '2024-02-20',
    numberOfBookableSeats: 9,
    itineraries: [
      {
        duration: 'PT12H',
        segments: [
          {
            departure: {
              iataCode: 'DEL',
              terminal: '3',
              at: '2024-02-27T02:05:00',
            },
            arrival: {
              iataCode: 'ZRH',
              at: '2024-02-27T06:20:00',
            },
            carrierCode: 'LX',
            number: '147',
            aircraft: {
              code: '333',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT8H45M',
            id: '1',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
          {
            departure: {
              iataCode: 'ZRH',
              at: '2024-02-27T07:55:00',
            },
            arrival: {
              iataCode: 'LCY',
              at: '2024-02-27T08:35:00',
            },
            carrierCode: 'LX',
            number: '460',
            aircraft: {
              code: '221',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT1H40M',
            id: '2',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
        ],
      },
      {
        duration: 'PT12H45M',
        segments: [
          {
            departure: {
              iataCode: 'LHR',
              terminal: '2',
              at: '2024-03-12T06:00:00',
            },
            arrival: {
              iataCode: 'ZRH',
              at: '2024-03-12T08:40:00',
            },
            carrierCode: 'LX',
            number: '345',
            aircraft: {
              code: '32N',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT1H40M',
            id: '11',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
          {
            departure: {
              iataCode: 'ZRH',
              at: '2024-03-12T12:00:00',
            },
            arrival: {
              iataCode: 'DEL',
              terminal: '3',
              at: '2024-03-13T00:15:00',
            },
            carrierCode: 'LX',
            number: '146',
            aircraft: {
              code: '333',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT7H45M',
            id: '12',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
        ],
      },
    ],
    price: {
      currency: 'EUR',
      total: '2738.85',
      base: '775.00',
      fees: [
        {
          amount: '0.00',
          type: 'SUPPLIER',
        },
        {
          amount: '0.00',
          type: 'TICKETING',
        },
      ],
      grandTotal: '2738.85',
    },
    pricingOptions: {
      fareType: ['PUBLISHED'],
      includedCheckedBagsOnly: true,
    },
    validatingAirlineCodes: ['LX'],
    travelerPricings: [
      {
        travelerId: '1',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'USD',
          total: '3680.00',
          base: '2409.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '3',
            cabin: 'BUSINESS',
            fareBasis: 'INW0A3S5',
            brandedFare: 'BUSOPTIMA',
            brandedFareLabel: 'BUSINESS OPTIMA',
            class: 'I',
            includedCheckedBags: {
              quantity: 2,
            },
            amenities: [
              {
                description: 'THIRD CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MEAL',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'SNACK OR DRINK',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PRIORITY CHECK IN',
                isChargeable: false,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'WIFI CONNECTION',
                isChargeable: true,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PRIORITY BOARDING GROUP 1',
                isChargeable: false,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '4',
            cabin: 'BUSINESS',
            fareBasis: 'INW0A3S5',
            brandedFare: 'BUSOPTIMA',
            brandedFareLabel: 'BUSINESS OPTIMA',
            class: 'I',
            includedCheckedBags: {
              quantity: 2,
            },
            amenities: [
              {
                description: 'THIRD CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MEAL',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'SNACK OR DRINK',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PRIORITY CHECK IN',
                isChargeable: false,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'WIFI CONNECTION',
                isChargeable: true,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PRIORITY BOARDING GROUP 1',
                isChargeable: false,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '13',
            cabin: 'BUSINESS',
            fareBasis: 'INW0A3S5',
            brandedFare: 'OPTIMA',
            brandedFareLabel: 'OPTIMA',
            class: 'J',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'SECOND CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'THIRD CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MEAL',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'SNACK OR DRINK',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'WIFI CONNECTION',
                isChargeable: true,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '14',
            cabin: 'ECONOMY',
            fareBasis: 'LLN0A0M5',
            brandedFare: 'OPTIMA',
            brandedFareLabel: 'OPTIMA',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'SECOND CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'THIRD CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MEAL',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'SNACK OR DRINK',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'WIFI CONNECTION',
                isChargeable: true,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '2',
        fareOption: 'STANDARD',
        travelerType: 'CHILD',
        price: {
          currency: 'USD',
          total: '3680.00',
          base: '2409.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '3',
            cabin: 'BUSINESS',
            fareBasis: 'INW0A3S5',
            brandedFare: 'BUSOPTIMA',
            brandedFareLabel: 'BUSINESS OPTIMA',
            class: 'I',
            amenities: [
              {
                description: 'THIRD CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MEAL',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'SNACK OR DRINK',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PRIORITY CHECK IN',
                isChargeable: false,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'WIFI CONNECTION',
                isChargeable: true,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PRIORITY BOARDING GROUP 1',
                isChargeable: false,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '4',
            cabin: 'BUSINESS',
            fareBasis: 'INW0A3S5',
            brandedFare: 'BUSOPTIMA',
            brandedFareLabel: 'BUSINESS OPTIMA',
            class: 'I',
            amenities: [
              {
                description: 'THIRD CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MEAL',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'SNACK OR DRINK',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PRIORITY CHECK IN',
                isChargeable: false,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'WIFI CONNECTION',
                isChargeable: true,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PRIORITY BOARDING GROUP 1',
                isChargeable: false,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '13',
            cabin: 'BUSINESS',
            fareBasis: 'INW0A3S5',
            brandedFare: 'OPTIMA',
            brandedFareLabel: 'OPTIMA',
            class: 'J',
            amenities: [
              {
                description: 'SECOND CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'THIRD CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MEAL',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'SNACK OR DRINK',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'WIFI CONNECTION',
                isChargeable: true,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '14',
            cabin: 'ECONOMY',
            fareBasis: 'LLN0A0M5',
            brandedFare: 'OPTIMA',
            brandedFareLabel: 'OPTIMA',
            class: 'L',
            amenities: [
              {
                description: 'SECOND CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'THIRD CHECKED BAG',
                isChargeable: true,
                amenityType: 'BAGGAGE',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MEAL',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'SNACK OR DRINK',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'WIFI CONNECTION',
                isChargeable: true,
                amenityType: 'TRAVEL_SERVICES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
    ],
  },
  {
    type: 'flight-offer',
    id: '2',
    source: 'GDS',
    instantTicketingRequired: false,
    nonHomogeneous: false,
    oneWay: false,
    lastTicketingDate: '2024-02-20',
    lastTicketingDateTime: '2024-02-20',
    numberOfBookableSeats: 9,
    itineraries: [
      {
        duration: 'PT14H40M',
        segments: [
          {
            departure: {
              iataCode: 'DEL',
              terminal: '3',
              at: '2024-02-27T02:05:00',
            },
            arrival: {
              iataCode: 'ZRH',
              at: '2024-02-27T06:20:00',
            },
            carrierCode: 'LX',
            number: '147',
            aircraft: {
              code: '333',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT8H45M',
            id: '7',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
          {
            departure: {
              iataCode: 'ZRH',
              at: '2024-02-27T10:20:00',
            },
            arrival: {
              iataCode: 'LHR',
              terminal: '2',
              at: '2024-02-27T11:15:00',
            },
            carrierCode: 'LX',
            number: '318',
            aircraft: {
              code: '32N',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT1H55M',
            id: '8',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
        ],
      },
      {
        duration: 'PT12H45M',
        segments: [
          {
            departure: {
              iataCode: 'LHR',
              terminal: '2',
              at: '2024-03-12T06:00:00',
            },
            arrival: {
              iataCode: 'ZRH',
              at: '2024-03-12T08:40:00',
            },
            carrierCode: 'LX',
            number: '345',
            aircraft: {
              code: '32N',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT1H40M',
            id: '11',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
          {
            departure: {
              iataCode: 'ZRH',
              at: '2024-03-12T12:00:00',
            },
            arrival: {
              iataCode: 'DEL',
              terminal: '3',
              at: '2024-03-13T00:15:00',
            },
            carrierCode: 'LX',
            number: '146',
            aircraft: {
              code: '333',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT7H45M',
            id: '12',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
        ],
      },
    ],
    price: {
      currency: 'EUR',
      total: '2738.85',
      base: '775.00',
      fees: [
        {
          amount: '0.00',
          type: 'SUPPLIER',
        },
        {
          amount: '0.00',
          type: 'TICKETING',
        },
      ],
      grandTotal: '2738.85',
    },
    pricingOptions: {
      fareType: ['PUBLISHED'],
      includedCheckedBagsOnly: true,
    },
    validatingAirlineCodes: ['LX'],
    travelerPricings: [
      {
        travelerId: '1',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '7',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '8',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '2',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '7',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '8',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '3',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '7',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '8',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '4',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '7',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '8',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '5',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '7',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '8',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
    ],
  },
  {
    type: 'flight-offer',
    id: '3',
    source: 'GDS',
    instantTicketingRequired: false,
    nonHomogeneous: false,
    oneWay: false,
    lastTicketingDate: '2024-02-20',
    lastTicketingDateTime: '2024-02-20',
    numberOfBookableSeats: 9,
    itineraries: [
      {
        duration: 'PT16H25M',
        segments: [
          {
            departure: {
              iataCode: 'DEL',
              terminal: '3',
              at: '2024-02-27T02:05:00',
            },
            arrival: {
              iataCode: 'ZRH',
              at: '2024-02-27T06:20:00',
            },
            carrierCode: 'LX',
            number: '147',
            aircraft: {
              code: '333',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT8H45M',
            id: '3',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
          {
            departure: {
              iataCode: 'ZRH',
              at: '2024-02-27T12:10:00',
            },
            arrival: {
              iataCode: 'LHR',
              terminal: '2',
              at: '2024-02-27T13:00:00',
            },
            carrierCode: 'LX',
            number: '332',
            aircraft: {
              code: '32Q',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT1H50M',
            id: '4',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
        ],
      },
      {
        duration: 'PT12H45M',
        segments: [
          {
            departure: {
              iataCode: 'LHR',
              terminal: '2',
              at: '2024-03-12T06:00:00',
            },
            arrival: {
              iataCode: 'ZRH',
              at: '2024-03-12T08:40:00',
            },
            carrierCode: 'LX',
            number: '345',
            aircraft: {
              code: '32N',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT1H40M',
            id: '11',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
          {
            departure: {
              iataCode: 'ZRH',
              at: '2024-03-12T12:00:00',
            },
            arrival: {
              iataCode: 'DEL',
              terminal: '3',
              at: '2024-03-13T00:15:00',
            },
            carrierCode: 'LX',
            number: '146',
            aircraft: {
              code: '333',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT7H45M',
            id: '12',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
        ],
      },
    ],
    price: {
      currency: 'EUR',
      total: '2738.85',
      base: '775.00',
      fees: [
        {
          amount: '0.00',
          type: 'SUPPLIER',
        },
        {
          amount: '0.00',
          type: 'TICKETING',
        },
      ],
      grandTotal: '2738.85',
    },
    pricingOptions: {
      fareType: ['PUBLISHED'],
      includedCheckedBagsOnly: true,
    },
    validatingAirlineCodes: ['LX'],
    travelerPricings: [
      {
        travelerId: '1',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '3',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '4',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '2',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '3',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '4',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '3',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '3',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '4',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '4',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '3',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '4',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '5',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '3',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '4',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
    ],
  },
  {
    type: 'flight-offer',
    id: '4',
    source: 'GDS',
    instantTicketingRequired: false,
    nonHomogeneous: false,
    oneWay: false,
    lastTicketingDate: '2024-02-20',
    lastTicketingDateTime: '2024-02-20',
    numberOfBookableSeats: 9,
    itineraries: [
      {
        duration: 'PT19H45M',
        segments: [
          {
            departure: {
              iataCode: 'DEL',
              terminal: '3',
              at: '2024-02-27T02:05:00',
            },
            arrival: {
              iataCode: 'ZRH',
              at: '2024-02-27T06:20:00',
            },
            carrierCode: 'LX',
            number: '147',
            aircraft: {
              code: '333',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT8H45M',
            id: '9',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
          {
            departure: {
              iataCode: 'ZRH',
              at: '2024-02-27T15:30:00',
            },
            arrival: {
              iataCode: 'LHR',
              terminal: '2',
              at: '2024-02-27T16:20:00',
            },
            carrierCode: 'LX',
            number: '324',
            aircraft: {
              code: '32N',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT1H50M',
            id: '10',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
        ],
      },
      {
        duration: 'PT12H45M',
        segments: [
          {
            departure: {
              iataCode: 'LHR',
              terminal: '2',
              at: '2024-03-12T06:00:00',
            },
            arrival: {
              iataCode: 'ZRH',
              at: '2024-03-12T08:40:00',
            },
            carrierCode: 'LX',
            number: '345',
            aircraft: {
              code: '32N',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT1H40M',
            id: '11',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
          {
            departure: {
              iataCode: 'ZRH',
              at: '2024-03-12T12:00:00',
            },
            arrival: {
              iataCode: 'DEL',
              terminal: '3',
              at: '2024-03-13T00:15:00',
            },
            carrierCode: 'LX',
            number: '146',
            aircraft: {
              code: '333',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT7H45M',
            id: '12',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
        ],
      },
    ],
    price: {
      currency: 'EUR',
      total: '2738.85',
      base: '775.00',
      fees: [
        {
          amount: '0.00',
          type: 'SUPPLIER',
        },
        {
          amount: '0.00',
          type: 'TICKETING',
        },
      ],
      grandTotal: '2738.85',
    },
    pricingOptions: {
      fareType: ['PUBLISHED'],
      includedCheckedBagsOnly: true,
    },
    validatingAirlineCodes: ['LX'],
    travelerPricings: [
      {
        travelerId: '1',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '9',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '10',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '2',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '9',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '10',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '3',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '9',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '10',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '4',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '9',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '10',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '5',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '9',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '10',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
    ],
  },
  {
    type: 'flight-offer',
    id: '5',
    source: 'GDS',
    instantTicketingRequired: false,
    nonHomogeneous: false,
    oneWay: false,
    lastTicketingDate: '2024-02-20',
    lastTicketingDateTime: '2024-02-20',
    numberOfBookableSeats: 9,
    itineraries: [
      {
        duration: 'PT20H30M',
        segments: [
          {
            departure: {
              iataCode: 'DEL',
              terminal: '3',
              at: '2024-02-27T02:05:00',
            },
            arrival: {
              iataCode: 'ZRH',
              at: '2024-02-27T06:20:00',
            },
            carrierCode: 'LX',
            number: '147',
            aircraft: {
              code: '333',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT8H45M',
            id: '5',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
          {
            departure: {
              iataCode: 'ZRH',
              at: '2024-02-27T16:25:00',
            },
            arrival: {
              iataCode: 'LCY',
              at: '2024-02-27T17:05:00',
            },
            carrierCode: 'LX',
            number: '464',
            aircraft: {
              code: '221',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT1H40M',
            id: '6',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
        ],
      },
      {
        duration: 'PT12H45M',
        segments: [
          {
            departure: {
              iataCode: 'LHR',
              terminal: '2',
              at: '2024-03-12T06:00:00',
            },
            arrival: {
              iataCode: 'ZRH',
              at: '2024-03-12T08:40:00',
            },
            carrierCode: 'LX',
            number: '345',
            aircraft: {
              code: '32N',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT1H40M',
            id: '11',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
          {
            departure: {
              iataCode: 'ZRH',
              at: '2024-03-12T12:00:00',
            },
            arrival: {
              iataCode: 'DEL',
              terminal: '3',
              at: '2024-03-13T00:15:00',
            },
            carrierCode: 'LX',
            number: '146',
            aircraft: {
              code: '333',
            },
            operating: {
              carrierCode: 'LX',
            },
            duration: 'PT7H45M',
            id: '12',
            numberOfStops: 0,
            blacklistedInEU: false,
          },
        ],
      },
    ],
    price: {
      currency: 'EUR',
      total: '2738.85',
      base: '775.00',
      fees: [
        {
          amount: '0.00',
          type: 'SUPPLIER',
        },
        {
          amount: '0.00',
          type: 'TICKETING',
        },
      ],
      grandTotal: '2738.85',
    },
    pricingOptions: {
      fareType: ['PUBLISHED'],
      includedCheckedBagsOnly: true,
    },
    validatingAirlineCodes: ['LX'],
    travelerPricings: [
      {
        travelerId: '1',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '5',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '6',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '2',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '5',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '6',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '3',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '5',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '6',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '4',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '5',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '6',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
      {
        travelerId: '5',
        fareOption: 'STANDARD',
        travelerType: 'ADULT',
        price: {
          currency: 'EUR',
          total: '547.77',
          base: '155.00',
        },
        fareDetailsBySegment: [
          {
            segmentId: '5',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '6',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '11',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
          {
            segmentId: '12',
            cabin: 'ECONOMY',
            fareBasis: 'LNCAA',
            brandedFare: 'ECOSAVER',
            brandedFareLabel: 'ECONOMY SAVER',
            class: 'L',
            includedCheckedBags: {
              quantity: 1,
            },
            amenities: [
              {
                description: 'EXTRA LEGROOM SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'PRE_RESERVED_SEAT',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON EUROPE AND CAI FLT',
                isChargeable: true,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'CATERING ON INTERCONT FLTS',
                isChargeable: false,
                amenityType: 'MEAL',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'STANDARD SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'MILEAGE ACCRUAL',
                isChargeable: false,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'UPGRADE ELIGIBILITY',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
              {
                description: 'PREFERRED SEAT RESERVATION',
                isChargeable: true,
                amenityType: 'BRANDED_FARES',
                amenityProvider: {
                  name: 'BrandedFare',
                },
              },
            ],
          },
        ],
      },
    ],
  },
];
